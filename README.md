# ussr.win hubzilla themes

- ussrbasic
- ussrbasicpub

This themes has Focus (wgite background) and GreenGrey (dark background) schemes. Also this themes has Nolikes scheme (hide "Likes" feature) and Nominus scheme (hide "Minus" icon under one-side connections).

Adapted for Hubzilla version 8.

- ussrbasic - default theme with custom GreenGrey schemes

![ussrbasic](https://dev.ussr.win/ussr/ussr-hubzilla-themes/raw/branch/master/ussrbasic/img/screenshot.png)

- ussrbasicpub - public GreenGrey theme with informative navbar

![ussrbasicpub](https://dev.ussr.win/ussr/ussr-hubzilla-themes/raw/branch/master/ussrbasicpub/img/screenshot.png)

You can use PDL Editor to create a two-column page layout.
