<?php
require_once('view/php/theme_init.php');

head_add_css('/library/fork-awesome/css/fork-awesome.min.css');
head_add_css('/vendor/twbs/bootstrap/dist/css/bootstrap.min.css');
head_add_css('/library/bootstrap-tagsinput/bootstrap-tagsinput.css');
head_add_css('/view/css/bootstrap-red.css');
head_add_css('/library/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css');

head_add_js('/vendor/twbs/bootstrap/dist/js/bootstrap.bundle.min.js');
head_add_js('/library/bootbox/bootbox.min.js');
head_add_js('/library/bootstrap-tagsinput/bootstrap-tagsinput.js');
head_add_js('/library/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js');

$ussrbasicpub_mode = '';
$ussrbasicpub_navbar_mode = '';

if (local_channel()) {
	$ussrbasicpub_mode = ((get_pconfig(local_channel(), 'ussrbasicpub', 'dark_mode')) ? 'dark' : 'light');
	$ussrbasicpub_navbar_mode = ((get_pconfig(local_channel(), 'ussrbasicpub', 'navbar_dark_mode')) ? 'dark' : 'light');
}

if (App::$profile_uid) {
	$ussrbasicpub_mode = ((get_pconfig(App::$profile_uid, 'ussrbasicpub', 'dark_mode')) ? 'dark' : 'light');
	$ussrbasicpub_navbar_mode = ((get_pconfig(App::$profile_uid, 'ussrbasicpub', 'navbar_dark_mode')) ? 'dark' : 'light');
}

if (!$ussrbasicpub_mode) {
	$ussrbasicpub_mode = ((get_config('ussrbasicpub', 'dark_mode')) ? 'dark' : 'light');
	$ussrbasicpub_navbar_mode = ((get_config('ussrbasicpub', 'navbar_dark_mode')) ? 'dark' : 'light');
}

App::$page['color_mode'] = 'data-bs-theme="' . $ussrbasicpub_mode . '"';
App::$page['navbar_color_mode'] = (($ussrbasicpub_navbar_mode === 'dark') ? 'data-bs-theme="' . $ussrbasicpub_navbar_mode . '"' : '');
